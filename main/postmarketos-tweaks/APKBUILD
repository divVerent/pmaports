# Maintainer: Martijn Braam <martijn@brixit.nl>
pkgname=postmarketos-tweaks
pkgver=0.8.0
pkgrel=0
pkgdesc="Tweak tool for phone UIs"
url="https://gitlab.com/postmarketOS/postmarketos-tweaks"
arch="noarch"
license="GPL-3.0-or-later"
subpackages="$pkgname-phosh:phosh $pkgname-pinephone:pinephone"
depends="python3 py3-gobject3 py3-yaml gtk+3.0 libhandy1"
makedepends="py3-setuptools glib-dev libhandy1-dev meson"
install="$pkgname.post-install $pkgname.post-upgrade"
source="$pkgname-$pkgver.tar.gz::https://gitlab.com/postmarketOS/postmarketos-tweaks/-/archive/$pkgver/postmarketos-tweaks-$pkgver.tar.gz"
options="!check" # There's no testsuite

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel phosh"

	amove usr/share/postmarketos-tweaks/phosh.yml
}

pinephone() {
	install_if="$pkgname=$pkgver-r$pkgrel device-pine64-pinephone"

	amove usr/share/postmarketos-tweaks/pinephone.yml
}

sha512sums="
8dbb002be2585491dea85ee139a128fce4ca98b7c83cf7643f71cb6c60fb98f6adc9f26eb31a52578f01989419a93d7d9bafde7ad77eb549aa08bf5564a0cc95  postmarketos-tweaks-0.8.0.tar.gz
"
